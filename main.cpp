#include "triangularMatrix.h"
#include "sparseMatrix.h"


int main() {
	

	

	cout << "Triangular matrix test:" << endl;
	cout << "Create a matrix 3x3\n" << endl;
	triangularMatrix A(3);
	A.Print();
	cout << endl;
	cout << "Create a vector with 3 elements:\n" << endl;
	vector<int> B = { 1, 2, 3 };
	for (int i = 0; i < 3; i++) cout << B[i] << " ";
	cout << endl;
	cout << endl;
	cout << "Multiply matrix by vector:\n" << endl;
	A.MultVector(B);
	A.Print();
	cout <<"\n"<< endl;
	cout << "Sparse matrix test:" << endl;
	cout << "Create a matrix 5x5 with 10 with non-zero elements:\n" << endl;
	sparseMatrix �(5, 10);
	�.PrintValues();
	cout << endl;
	cout << "Create vector with 10 elements:\n" << endl;
	vector<int> D = { 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 };
	for (int i = 0; i < 10; i++) cout << D[i] << " ";
	cout << endl;
	cout << endl;
	cout << "Multiply matrix by vector:\n" << endl;
	�.Multiplicate(D);
	�.PrintValues();
	getchar();
	return 0;
}