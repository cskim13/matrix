#pragma once
#include <iostream>
#include <vector>
using namespace std;

class sparseMatrix {

	int Size; 
	int NonZero; 
	int* Value; 
	int* Column; 
	int* RowIndex; 

public:
	sparseMatrix(int _N, int _NonZero) {
		Size = _N;
		NonZero = _NonZero;
		Value = new int[NonZero];
		Column = new int[NonZero];
		RowIndex = new int[Size + 1];

		for (int i = 0; i < NonZero; i++) Value[i] = rand() % 10;
		for (int i = 0; i < NonZero; i++) Column[i] = 1 + rand() % Size;
		for (int i = 0; i < NonZero; i++) RowIndex[i] = 1 + rand() % Size;
	}

	void Multiplicate(vector<int> B)
	{
		for (int i = 0; i < Size; i++) {
			for (int j = 0; j < NonZero; j++) if (RowIndex[j] == i) Value[j] *= B[i];
		}
	}

	void PrintValues() {
		for (int i = 0; i < NonZero; i++) cout << Value[i] << " ";
		cout << endl;
	}
};